export default () => ({
  server_port: parseInt(process.env.SERVER_PORT, 10) || 3000,
  db_url: process.env.DB_URL,
  db_port: process.env.DB_PORT,
  db_mongo_connection: process.env.DB_MONGO_CONNECTION,
  db_root_user: process.env.DB_ROOT_USER,
  db_root_pw: process.env.DB_ROOT_PW,
  db_host: process.env.DB_HOST,
  db_name: process.env.DB_NAME,
  secret_jwt_key: process.env.SECRET_JWT_KEY,
});
