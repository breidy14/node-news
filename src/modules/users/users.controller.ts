import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
} from '@nestjs/common';
import { ApiTags, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';

import { MongoIdPipe } from '../../common/mongo-id.pipe';
import { UsersService } from './users.service';
import { CreateUserDto, UpdateUserDto } from './dto';
import { Public } from '../auth/decorators/public.decorator';
import { User } from './schemas/user.schema';

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Public()
  @Post()
  @ApiOperation({ summary: 'Create user' })
  create(@Body() createUserDto: CreateUserDto): Promise<User> {
    const user = this.usersService.create(createUserDto);
    return user;
  }

  @Get()
  @ApiOperation({ summary: 'List of users' })
  @ApiBearerAuth()
  findAll() {
    const users = this.usersService.findAll();
    return users;
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find one user by id' })
  @ApiBearerAuth()
  findOne(@Param('id', MongoIdPipe) id: string): Promise<User> {
    const user = this.usersService.findOne(id);
    return user;
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update user by id' })
  @ApiBearerAuth()
  update(
    @Param('id', MongoIdPipe) id: string,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<User> {
    const user = this.usersService.update(id, updateUserDto);
    return user;
  }

  @Delete(':id')
  @HttpCode(204)
  @ApiOperation({ summary: 'Delete user by id' })
  @ApiBearerAuth()
  remove(@Param('id', MongoIdPipe) id: string) {
    this.usersService.remove(id);
  }
}
