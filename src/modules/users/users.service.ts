import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';

import { CreateUserDto, UpdateUserDto } from './dto';
import { User } from './schemas/user.schema';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}
  async create(createUserDto: CreateUserDto): Promise<User> {
    let userDB = await this.userModel.findOne({ email: createUserDto.email });
    if (!userDB) {
      throw new BadRequestException(
        `Already exist user whit email: ${createUserDto.email}`,
      );
    }

    userDB = await this.userModel.findOne({ userName: createUserDto.userName });
    if (!userDB) {
      throw new BadRequestException(
        `Already exist user whit username: ${createUserDto.userName}`,
      );
    }

    const createdUser = new this.userModel(createUserDto);

    const salt = bcrypt.genSaltSync();
    createdUser.password = await bcrypt.hash(createdUser.password, salt);
    await createdUser.save();

    return createdUser;
  }

  async findAll(): Promise<User[]> {
    const users = await this.userModel.find();
    return users;
  }

  async findOne(id: string): Promise<User> {
    const user = await this.userModel.findById(id);

    if (!user) {
      throw new NotFoundException(`User #${id} not found`);
    }

    return user;
  }

  async validateId(id: string): Promise<User> {
    const user = await this.userModel.findById(id);

    if (!user) {
      return null;
    }

    return user;
  }

  async findByUserName(userName: string): Promise<any> {
    const user = await this.userModel.findOne({ userName }).lean(true);

    if (!user) {
      return null;
    }

    return user;
  }

  async update(
    id: string,
    { password, ...updateUserDto }: UpdateUserDto,
  ): Promise<User> {
    const user = await this.userModel.findByIdAndUpdate(id, updateUserDto, {
      new: true,
    });

    if (!user) {
      throw new NotFoundException(`User #${id} not found`);
    }

    return user;
  }

  async remove(id: string) {
    const user = await this.userModel.findByIdAndDelete(id);

    if (!user) {
      throw new NotFoundException(`User #${id} not found`);
    }
  }
}
