import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class NewsDelete extends Document {
  @Prop({ name: 'object_id', unique: true })
  objectID: string;
}

export const NewsDeleteSchema = SchemaFactory.createForClass(NewsDelete);
