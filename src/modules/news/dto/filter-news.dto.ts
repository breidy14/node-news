import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsString, IsOptional } from 'class-validator';

export class FilterNewsDto {
  @IsOptional()
  @ApiPropertyOptional()
  limit: string;

  @IsOptional()
  @ApiPropertyOptional()
  offset: string;

  @IsOptional()
  @ApiPropertyOptional()
  @IsString()
  _tags: string;

  @IsOptional()
  @ApiPropertyOptional()
  @IsString()
  author: string;

  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @ApiPropertyOptional()
  @IsString()
  month: string;
}
