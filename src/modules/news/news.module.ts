import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HttpModule } from '@nestjs/axios';

import { NewsService } from './news.service';
import { NewsController } from './news.controller';
import { News, NewsSchema } from './models/news.schema';
import { NewsDelete, NewsDeleteSchema } from './models/news-delete.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: News.name, schema: NewsSchema },
      { name: NewsDelete.name, schema: NewsDeleteSchema },
    ]),
    HttpModule,
  ],
  controllers: [NewsController],
  providers: [NewsService],
  exports: [NewsService],
})
export class NewsModule {}
