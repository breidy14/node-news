import { HttpService } from '@nestjs/axios';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';

import { Model } from 'mongoose';
import { FilterNewsDto } from './dto/filter-news.dto';
import { NewsDelete } from './models/news-delete.schema';
import { News } from './models/news.schema';

@Injectable()
export class NewsService {
  constructor(
    @InjectModel(News.name) private newsModel: Model<News>,
    @InjectModel(NewsDelete.name) private newsDeleteModel: Model<NewsDelete>,
    private httpService: HttpService,
  ) {}

  @Cron(CronExpression.EVERY_HOUR)
  async cronRequest() {
    const news = await this.upLoadData(true);
    if (news.length === 0) {
      console.log('Error upload news data');
    } else {
      console.log('News upload ready');
    }
  }

  async findAll(params?: FilterNewsDto): Promise<News[]> {
    const monthNums = {
      january: 1,
      february: 2,
      march: 3,
      april: 4,
      may: 5,
      june: 6,
      july: 7,
      august: 8,
      september: 9,
      october: 10,
      november: 11,
      december: 12,
    };
    const { limit = 5, offset = 0 } = params;
    if (params) {
      const filters: any = {};
      const { author, _tags, title, month } = params;

      if (author) {
        filters.author = author;
      }
      if (_tags) {
        filters._tags = _tags;
      }
      if (title) {
        const newTitle = title.replace('_', ' ');
        const regex = new RegExp(`${newTitle}`, 'i');
        /*para el filtro por title use el campo story_title,
          para que se vea la funcionalidad del filtro,
          ya que title siempre viene en null
        */
        filters.story_title = { $regex: regex };
      }
      if (month) {
        return await this.newsModel
          .aggregate([
            {
              $match: {
                $or: [
                  filters,
                  {
                    $expr: {
                      $eq: [
                        { $month: '$created_at' },
                        monthNums[month.toLowerCase()],
                      ],
                    },
                  },
                ],
              },
            },
          ])
          .skip(Number(offset))
          .limit(Number(limit))
          .exec();
      }

      return await this.newsModel
        .aggregate([{ $match: { $or: [filters] } }])
        .skip(Number(offset))
        .limit(Number(limit))
        .exec();
    }

    return await this.newsModel
      .find()
      .skip(Number(offset))
      .limit(Number(limit))
      .exec();
  }

  async findOne(objectID: string): Promise<News> {
    const news = await this.newsModel.findOne({ objectID });

    if (!news) {
      throw new NotFoundException(`News #${objectID} not found`);
    }

    return news;
  }

  async remove(objectID: string): Promise<void> {
    const news = await this.newsModel.findOneAndDelete({ objectID });
    if (!news) {
      throw new NotFoundException(`News #${objectID} not found`);
    }
    const newsDelete = new this.newsDeleteModel({ objectID });
    await newsDelete.save();
  }

  async forceUpLoadData(): Promise<string> {
    const news = await this.upLoadData(false);
    if (news.length === 0) {
      console.log('Error upload news data');
      return 'Error upload news data';
    } else {
      console.log('News data ready');
      return 'News data ready';
    }
  }

  async upLoadData(isCron?: boolean): Promise<News[]> {
    let newsDB: News[];
    if (!isCron) {
      newsDB = await this.newsModel.find();
      if (newsDB.length !== 0) {
        return newsDB;
      }
    }
    const { data } = await this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();

    newsDB = await this.newsModel.find();
    const newsDeleteDB = await this.newsDeleteModel.find();
    const arrObjectID = [];

    if (newsDB.length !== 0) {
      newsDB.forEach((el) => arrObjectID.push(el.objectID));
    }

    if (newsDeleteDB.length !== 0) {
      newsDeleteDB.forEach((el) => arrObjectID.push(el.objectID));
    }

    const newData = data.hits.filter((news) => {
      if (!arrObjectID.includes(news.objectID)) {
        return news;
      }
    });
    if (newData.length !== 0) {
      const news = await this.newsModel.insertMany(newData);

      return news;
    }

    return newsDB;
  }
}
