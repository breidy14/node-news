import {
  Controller,
  Get,
  Param,
  Delete,
  Query,
  HttpCode,
} from '@nestjs/common';
import { ApiTags, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';

import { NewsService } from './news.service';
import { FilterNewsDto } from './dto/filter-news.dto';
import { News } from './models/news.schema';

@ApiTags('News')
@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @Get()
  @ApiOperation({ summary: 'List of news' })
  @ApiBearerAuth()
  findAll(@Query() params: FilterNewsDto): Promise<News[]> {
    return this.newsService.findAll(params);
  }

  @Get('upload-data')
  @ApiOperation({ summary: 'Force data upload' })
  @ApiBearerAuth()
  loadNews() {
    return this.newsService.forceUpLoadData();
  }

  @Delete(':objectID')
  @HttpCode(204)
  @ApiOperation({
    summary: 'Delete news by objectID',
    description: 'Delete news by objectID, which bring the news',
  })
  @ApiBearerAuth()
  remove(@Param('objectID') objectID: string) {
    this.newsService.remove(objectID);
  }
}
