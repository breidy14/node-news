import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(userName: string, pass: string): Promise<any> {
    const user = await this.usersService.findByUserName(userName);

    if (!user) {
      return null;
    }

    const validpass = await bcrypt.compare(pass, user.password);
    if (!validpass) {
      return null;
    }

    const { password, ...result } = user;
    return result;
  }

  async login(user: any) {
    const payload = { username: user.userName, sub: user._id };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
