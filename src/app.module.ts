/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';

import config from './config/config';

import {
  DB_HOST,
  DB_MONGO_CONNECTION,
  DB_NAME,
  DB_PORT,
  DB_ROOT_PW,
  DB_ROOT_USER,
  SERVER_PORT,
} from './config/constants';

import { UsersModule } from './modules/users/users.module';
import { AuthModule } from './modules/auth/auth.module';
import { NewsModule } from './modules/news/news.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        const db_mongo_connection =
          configService.get<string>(DB_MONGO_CONNECTION);
        const db_host = configService.get<string>(DB_HOST);
        const db_port = configService.get<string>(DB_PORT);
        const db_root_pw = configService.get<string>(DB_ROOT_PW);
        const db_root_user = configService.get<string>(DB_ROOT_USER);
        const db_name = configService.get<string>(DB_NAME);
        return {
          uri: `${db_mongo_connection}://${db_host}:${db_port}`,
          user: db_root_user,
          pass: db_root_pw,
          dbName: db_name,
        };
      },
      inject: [ConfigService],
    }),
    ScheduleModule.forRoot(),
    AuthModule,
    NewsModule,
    UsersModule,
  ],
})
export class AppModule {
  static port: number | string;

  constructor(private readonly _configService: ConfigService) {
    AppModule.port = this._configService.get<string>(SERVER_PORT);
  }
}
