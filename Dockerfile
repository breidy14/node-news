FROM node:16.13.0-alpine3.12 AS development

WORKDIR /usr/src/app

COPY package.json ./
COPY package-lock.json ./

RUN npm install --only=development

COPY . .

RUN npm run build

EXPOSE 3000

CMD ["npm", "run", "start:dev"]
